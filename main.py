import json
import os

from icrawler.builtin import GreedyImageCrawler

CONFIG_FILE = "./config.json"


def main():
    with open(CONFIG_FILE, 'r') as cf:
        try:
            config_settings = json.load(cf)
        except NameError:
            print('Error en el formato del archivo de configuracion.')
            exit()

    storage = os.path.join(*config_settings.get("path"))

    greedy_crawler = GreedyImageCrawler(
        storage={
            'root_dir': storage
        },
        **config_settings.get("crawler_settings")
    )

    greedy_crawler.crawl(**config_settings.get("crawl_settings"))


if __name__ == '__main__':
    main()
